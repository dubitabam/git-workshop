#!/bin/bash

BASE_DIR=$(cd $(dirname $0); pwd -P)

. $BASE_DIR/scripts/utils.sh
cd $BASE_DIR

trap cancel INT
cancel () {
    cleanup
    exit 0
}

check_for_git


greeting () {
    echo
    echo "                   Welcome to our"
    echo "                    interactive"
    echo "                           __       "
    echo "                        __/\ \__    "
    echo "                    __ /\_\ \ ,_\   "
    echo "                  /'_ \`\/\ \ \ \/   "
    echo "                 /\ \L\ \ \ \ \ \_  "
    echo "                 \ \____ \ \_\ \__\ "
    echo "                  \/___L\ \/_/\/__/ "
    echo "                    /\____/         "
    echo "                    \_/__/        "
    echo
    echo "                       Course"
    echo
    echo
    echo "You can enter your own commands or leave the prompt blank"
    echo "to use the defaults."
    echo
    echo "Use <CTRL> + <C> at any point to cancel."
    echo
}

deep_thought () {
    echo
    echo "     ____________________________ "
    echo "    !\_________________________/!\ "
    echo "    !!                         !! \ "
    echo "    !!          Whoa!          !!  \ "
    echo "    !!                         !!  ! "
    echo "    !!       That is some      !!  ! "
    echo "    !!                         !!  ! "
    echo "    !!       DEEP THOUGHT      !!  ! "
    echo "    !!                         !!  ! "
    echo "    !!                         !!  / "
    echo "    !!_________________________!! / "
    echo "    !/_________________________\!/ "
    echo "       __\_________________/__/!_ "
    echo "      !_______________________!/ "
    echo
}


lecture_choice () {
    echo
    echo "[0] Exit"
    echo "[1] Exit without cleanup"
    echo "[2] Create repositories and exit"
    echo "[3] Simple Hotfix                       (easy)"
    echo "[4] Hotfix with Solo-Feature Branch     (mediocre)"
    echo "[5] Hotfix with Shared-Feature Branch   (advanced)"
    echo
    echo -n "Please select a lecture: "
    read INPUT

    if [ -z "$INPUT" ]; then
        cleanup
        exit 0
    fi

    case "$INPUT" in
        0)
        cleanup
        exit 0
        ;;
        1) exit 0 ;;
        2)
        init_repos
        exit 0
        ;;
        3)
        . $BASE_DIR/scripts/hotfix.sh
        ;;
        4)
        . $BASE_DIR/scripts/hotfix-feature.sh
        ;;
        5)
        . $BASE_DIR/scripts/hotfix-feature-shared.sh
        ;;
        42)
        deep_thought
        ;;
        *)
        echo
        echo "You're wrong... deadly wrong... try again."

        ;;
    esac

    press_enter
    lecture_choice
}


greeting
lecture_choice
cleanup
exit 0
