#
# colors
#
clroff="\033[0m"

# Regular Colors
black="\033[0;30m"
red="\033[0;31m"
green="\033[0;32m"
yellow="\033[0;33m"
blue="\033[0;34m"
purple="\033[0;35m"
cyan="\033[0;36m"
white="\033[0;37m"

# background
blackbg="\033[40m"
redbg="\033[41m"
greenbg="\033[42m"
yellowbg="\033[43m"
bluebg="\033[44m"
purplebg="\033[45m"
cyanbg="\033[46m"
whitebg="\033[47m"

# high intensty
iblack="\033[0;90m"
ired="\033[0;91m"
igreen="\033[0;92m"
iyellow="\033[0;93m"
iblue="\033[0;94m"
ipurple="\033[0;95m"
icyan="\033[0;96m"
iwhite="\033[0;97m"



check_for_git () {
    which git &> /dev/null
    if [ $? -ne 0 ]; then
        echo
        echo "git not found..."
        echo "how do you expect to run a git course without git... doh..."
        echo "Please make a fire and die in it... immediatly..."
        exit 1
    fi
}


gitlog () {
    git log --graph --pretty=format:'%Cred%h%Creset - %Cgreen(%cd)%Creset %C(bold blue)<%an> %C(yellow)%d %Creset%s%Creset' --abbrev-commit -n10 --date=short $1
}


get_current_branch () {
    echo `git status | grep -i "on branch" | cut -d " " -f3`
}


is_dirty () {
    if [ -z "git status --porcelain" ]; then
        return 1
    else
        return 0
    fi
}


get_commit_count () {
    echo `git log --pretty=oneline -n1000 | wc -l`
}


is_pushed () {
:
}


starts_with () {
    local cmd="$1"
    local val="$2"
    if [ "${cmd:0:${#val}}" == "$val" ]; then
        return 0
    else
        return 1
    fi
}


print_info () {
    echo -e "${green}// $1$clroff"
}


press_enter () {
    echo -en "${cyan}[PRESS ENTER TO CONTINUE]$clroff"
    read INPUT
}


exec_input () {
    local default="$1"

    echo "> ${default}"
    echo -n "$ "
    read INPUT

    if [ -z "$INPUT" ]; then
        INPUT="$default"
    fi

    if [ "$INPUT" == "boring" ] || [ "$INPUT" == "skip" ]; then
        if [ -z "$2" ]; then
            echo "No no no... you can't skip this step!"
            exec_input "$1" "$2"
        else
            return 99
        fi
    else

        echo -en "${black}${yellowbg}"
        eval $INPUT
        echo -e "$clroff\n"

        if [ $? -ne 0 ]; then
            echo -e "${red}there was an error. aborting course..."
            exit 1
        fi

        return 0
    fi
}


get_last_hash () {
    echo `git log --pretty=oneline -n1 | cut -d " " -f1`
}


init_repos () {
    cleanup
    mkdir local
    mkdir origin
    mkdir remote

    cd origin
    git init --bare > /dev/null

    cd ..
    cd local
    git init > /dev/null
    git config user.name WorkshopUser
    git config user.email git@workshop.user
    echo "Line 1 - Main App" > main.txt
    git add -A
    git commit -m "initial commit" > /dev/null
    git remote add origin ../origin
    git push origin master > /dev/null
    git checkout -b dev > /dev/null
    git push origin dev > /dev/null
    cd ..

    cd remote
    git init > /dev/null
    git config user.name RemoteUser
    git config user.email git@remote.user
    git remote add origin ../origin
    git pull origin master > /dev/null
    git fetch origin > /dev/null
    git checkout dev > /dev/null
    cd ..

    echo repositories created...
}


cleanup () {
    cd $BASE_DIR
    if [ -d ./origin ]; then
        rm -rf origin
    fi

    if [ -d ./local ]; then
        rm -rf ./local
    fi

    if [ -d ./remote ]; then
        rm -rf ./remote
    fi
}
