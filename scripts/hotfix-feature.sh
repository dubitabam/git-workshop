#!/bin/bash

echo
print_info "Setting up lecture..."
print_info "Ignore everything with blue background..."
echo
press_enter
echo -e "$bluebg"


# setup repos
cd $BASE_DIR
init_repos

# dev should be ahead of master
cd local
git checkout dev
echo "Line 2 - Some changes" >> main.txt
git add -A
git commit -m "Feat: added line 2"
git push origin dev
git checkout -b dev_Feature
echo "Line 3 - my awesome feature" >> main.txt
git add -A
git commit -m "Feat: my awesome feature"
git push origin dev_Feature
git checkout dev
cd ..


echo -e "$clroff"
echo
print_info "Lecture Setup completed."
print_info "Creating a hotfix with feature branch..."
echo
echo
print_info "This is how the current dev working tree looks."

cd local
gitlog

echo
print_info "Checkout the master branch."
print_info "This is what should have been deployed to the customer..."
echo
exec_input "git checkout master"

echo
print_info "Create a hotfix branch."
echo
exec_input "git checkout -b dev_Hotfix"

echo
print_info "Now you'd do your fix."
print_info "We'll do that for you :p"
echo
print_info "Adding hotfix code to main.txt"
echo
echo "Line 2 - Hotfix code..." >> main.txt
press_enter

echo
print_info "Commit your changes."
echo
exec_input "git add -A && git commit -m \"Fix: some hotfix\""
gitlog
commit=`get_last_hash`

echo
print_info "Now you'd build and deploy the hotfix branch..."
echo
press_enter

echo
print_info "Now we need to get the hotfix back to our dev working tree"
print_info "without merging or rewriting the history."
print_info "Therefore we cherry pick the hotfix changes into dev..."
echo
print_info "Checkout the dev branch."
echo
exec_input "git checkout dev"

echo
print_info "Cherry pick the hotfix commit into dev."
echo
exec_input "git cherry-pick ${commit}"

echo
print_info "Yep... you've a conflict..."
print_info "Lets resolve it. Put the hotfix change in line 2 and change line 2 to line 3."
print_info "Or insert 'boring' and we resolve it for you..."
echo
exec_input "vi main.txt" 1
if [ $? -eq 99 ]; then
    echo "Yes you are!"
    echo "Line 1 - Main App" > main.txt
    echo "Line 2 - Hotfix code..." >> main.txt
    echo "Line 3 - Some changes" >> main.txt
fi

echo
print_info "Commit the resolved conflicts."
echo
exec_input "git add main.txt && git commit -m \"Fix: some hotfix\""

echo
print_info "Push the dev branch to origin."
echo
exec_input "git push origin dev"
gitlog --all

echo
print_info "Whoa... this looks awful..."
print_info "Don't worry. We'll take of that."
echo
print_info "Now you can delete the hotfix branch."
print_info "As we never merged the hotfix commit and the cherry picked commit"
print_info "has a new hash, we need to use -D"
echo
exec_input "git branch -D dev_Hotfix"
gitlog --all

echo
print_info "Since you're currently working on a feature, we need to get"
print_info "your feature branch in line. Checkout your feature branch."
print_info "Only do this if no one else is working on that feature branch!"
echo
exec_input "git checkout dev_Feature"

echo
print_info "Rebase your feature branch onto dev."
echo
exec_input "git rebase dev"

echo
print_info "Damn! You've got a conflict... again..."
print_info "Yes it's annoying... but deal with it."
echo
print_info "Resolve the conflict."
print_info "Just change your feature line from 3 to 4."
print_info "Or insert 'boring' ..."
echo
exec_input "vi main.txt" 1
if [ $? -eq 99 ]; then
    echo "You're an only child right?"
    echo "Line 1 - Main App" > main.txt
    echo "Line 2 - Hotfix code..." >> main.txt
    echo "Line 3 - Some changes" >> main.txt
    echo "Line 4 - my awesome feature" >> main.txt
fi

echo
print_info "Alright... Continue the rebasing."
echo
exec_input "git add main.txt && git rebase --continue"
gitlog --all

echo
print_info "Looks better, doesn't it?"
echo
print_info "If you've pushed your feature branch to a remote (origin),"
print_info "you'll have to do a force push."
echo
print_info "Never ever do this if someone else is working on that branch!!!"
echo
exec_input "git push --force origin dev_Feature"
gitlog --all

echo
print_info "Congrats! You've made a hotfix!"
print_info "And all branches are in line!"
echo
