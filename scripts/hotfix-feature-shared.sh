#!/bin/bash

echo
print_info "Setting up lecture..."
print_info "Ignore everything with blue background..."
echo
press_enter
echo -e "$bluebg"


# setup repos
cd $BASE_DIR
init_repos

# dev should be ahead of master
cd local
git checkout dev
echo "Line 2 - Some changes" >> main.txt
git add -A
git commit -m "Feat: added line 2"
git push origin dev
git checkout -b dev_Feature
echo "Line 3 - my awesome feature" >> main.txt
git add -A
git commit -m "Feat: my awesome feature"
git push origin dev_Feature
git checkout dev
cd ..

# changes to feature from remote user
cd remote
git fetch origin
git checkout dev_Feature
echo "Line 4 - improved your feature... by a thousand percent!" >> main.txt
git add -A
git commit -m "Feat: improved feature by 1000%"
git push origin dev_Feature
git checkout dev
cd ..


echo -e "$clroff"
echo
print_info "Lecture Setup completed."
print_info "Creating a hotfix with a shared feature branch..."
echo
echo
print_info "This is how the current dev working tree looks."

cd local
gitlog

echo
print_info "Checkout the master branch."
print_info "This is what should have been deployed to the customer..."
echo
exec_input "git checkout master"

echo
print_info "Create a hotfix branch."
echo
exec_input "git checkout -b dev_Hotfix"

echo
print_info "Now you'd do your fix."
print_info "We'll do that for you :p"
echo
print_info "Adding hotfix code to main.txt"
echo
echo "Line 2 - Hotfix code..." >> main.txt
press_enter

echo
print_info "Commit your changes."
echo
exec_input "git add -A && git commit -m \"Fix: some hotfix\""
gitlog
commit=`get_last_hash`

echo
print_info "Now you'd build and deploy the hotfix branch..."
echo
press_enter

echo
print_info "Now we need to get the hotfix back to our dev working tree"
print_info "without merging or rewriting the history."
print_info "Therefore we cherry pick the hotfix changes into dev..."
echo
print_info "Checkout the dev branch."
echo
exec_input "git checkout dev"

echo
print_info "Cherry pick the hotfix commit into dev."
echo
exec_input "git cherry-pick ${commit}"

echo
print_info "Yep... you've a conflict..."
print_info "Lets resolve it. Put the hotfix change in line 2 and change line 2 to line 3."
print_info "Or insert 'boring' and we resolve it for you..."
echo
exec_input "vi main.txt" 1
if [ $? -eq 99 ]; then
    echo "Yes you are!"
fi
echo "Line 1 - Main App" > main.txt
echo "Line 2 - Hotfix code..." >> main.txt
echo "Line 3 - Some changes" >> main.txt

echo
print_info "Commit the resolved conflicts."
echo
exec_input "git add main.txt && git commit -m \"Fix: some hotfix\""

echo
print_info "Push the dev branch to origin."
echo
exec_input "git push origin dev"
gitlog --all

echo
print_info "Whoa... this looks awful..."
print_info "Don't worry. We'll take of that."
echo
print_info "Now you can delete the hotfix branch."
print_info "As we never merged the hotfix commit and the cherry picked commit"
print_info "has a new hash, we need to use -D"
echo
exec_input "git branch -D dev_Hotfix"
gitlog --all

echo
print_info "Wait... You're working on a feature and need that hotfix in it!"
print_info "But since you're not the only person working on that feature, we"
print_info "can't just use rebase cause this will rewrite the history..."
echo
print_info "So what should we do???"
echo
print_info "We merge... yep you heared right... MERGE"
print_info "This is the only scenario in which we'll use merge! (if it's not a fast forward)"
echo
exec_input "git checkout dev_Feature"

echo
print_info "Alright... merge dev into your feature branch."
echo
exec_input "git merge dev"

echo
print_info "Damn! You've got a conflict... again..."
print_info "Yes it's annoying... but deal with it."
echo
print_info "Resolve the conflict."
print_info "Use everything from dev, change line 3 from the feature branch to line 4 and discard the rest."
print_info "Or insert 'boring' ..."
echo
exec_input "vi main.txt" 1
if [ $? -eq 99 ]; then
    echo "Do your friends actually like you?"
fi
echo "Line 1 - Main App" > main.txt
echo "Line 2 - Hotfix code..." >> main.txt
echo "Line 3 - Some changes" >> main.txt
echo "Line 4 - my awesome feature" >> main.txt

echo
print_info "Ok... commit the resolved conflict"
echo
exec_input "git add main.txt && git commit"
gitlog --all

echo
print_info "Looks like shit? Yap but not for long."
print_info "Push your feature branch to origin."
echo
exec_input "git push origin dev_Feature"

echo
print_info "WTF?!?"
print_info "Doh... there are other people working on that feature too..."
print_info "And they've made changes meanwhile. We need to pull their changes first."
echo
print_info "DO NOT USE --force! Or you and everyone you love will get killed!"
echo
exec_input "git pull --rebase origin dev_Feature"

echo
print_info "Holy crap!!!"
print_info "Another conflict... what is this shit..."
print_info "Actually it's your own fault. You should have pulled before merging in the hotfix."
print_info "Now get to work and resolve..."
print_info "Or is it too 'boring' ..."
echo
exec_input "vi main.txt" 1
if [ $? -eq 99 ]; then
    echo "There's a special place in hell for you!"
fi
echo "Line 1 - Main App" > main.txt
echo "Line 2 - Hotfix code..." >> main.txt
echo "Line 3 - Some changes" >> main.txt
echo "Line 4 - my awesome feature" >> main.txt
echo "Line 5 - improved your feature... by a thousand percent!" >> main.txt

echo
print_info "Done? Good... what you're waiting for? Continue to rebase!"
print_info "Rebase? Yes! Forgot the --rebase in the pull command?"
print_info "And no... that's not the reason for the conflict."
print_info "You would've got that anyway. The --rebase will make it easier for you in the long run."
echo
exec_input "git add main.txt && git rebase --continue"
gitlog --all

echo
print_info "Well... this looks a bit weird..."
print_info "dev has the hotfix... your feature has the hotfix and everything dev has."
print_info "Why have they diverged?"
echo
print_info "Isn't it obvious? Forgot the --rebase again?"
print_info "Yes you have rewritten the history of your feature branch."
print_info "So of course they diverged..."
echo
print_info "And where is the merge commit? Bow to the might of rebase!"
echo
print_info "But let me read your mind: fuckin rebase... what's it good for anyway???"
print_info "Just so that a merge commit disappears?"
print_info "No... because if you only use merge, your repository will soon look like this..."
echo
press_enter
echo
echo "* | |   562dc9ce - Merge branch 'dev_dummy1' into dev_dummy2"
echo "|\ \ \\"
echo "| * \ \   e450912f - Merge branch 'dev_dummy3' into dev_dummy2"
echo "| |\ \ \\"
echo "| | * | | 8b49fe5 - some fix"
echo "| | * | |   fe00222d - Merge branch 'dev_somebranch' into dev_dummy3"
echo "| | |\ \ \\"
echo "| | * \ \ \   3b87109b - Merge branch 'dev' into dev_dummy3"
echo "| | |\ \ \ \\"
echo "| | | | |/ /"
echo "| | | |/| |"
echo "| | | * | | e173f783 - some other fix"
echo "| * | | | | 9c3a6b88 - changed a line"
echo "| * | | | |   033f56a3 - Merge branch 'dev_dummy2' into dev_dummy2"
echo "| |\ \ \ \ \\"
echo "| | | |_|/ /"
echo "| | |/| | |"
echo "| * | | | |   804b9123 - Merge branch 'dev' into dev_dummy2"
echo "| |\ \ \ \ \\"
echo "| | | |_|_|/"
echo "| | |/| | |"
echo "| | * | | | 4a81f992 - added a line"
echo "| | * | | |   fcd51572 - Merge branch 'dev' into dev"
echo "| | |\ \ \ \\"
echo "| | | | |_|/"
echo "| | | |/| |"
echo "| | * | | | 00edd75d - fixed some bug"
echo "| * | | | |   1ad49162 - Merge branch 'dev_dummy2' into dev_dummy2"
echo "| |\ \ \ \ \\"
echo "| * \ \ \ \ \   0fff91bf - Merge branch 'dev_dummy4' into dev_dummy2"
echo "| |\ \ \ \ \ \\"
echo "| | * | | | | | f3d79a5 - added missing line"
echo "| * | | | | | |   2a825f0 - Merge branch 'dev_dummy4' into dev_dummy2"
echo "| |\ \ \ \ \ \ \\"
echo "| | |/ / / / / /"
echo "| | * | | | | | 88ba80b - fix 1"
echo "| | * | | | | | 8cc77da - fix 1"
echo
print_info "This is a real life example... and much worse if you look at the whole history."
print_info "Who can tell what is going on there? His name is nobody..."
print_info "That is how a repository looks like if people don't know how too use git..."
echo
press_enter

echo
print_info "Anyway... we still need to get our feature and dev branch back in line."
print_info "We can only do that if everyone has finished their work on that feature."
print_info "Luckily for us we only need one small change and the feature is finished."
echo
print_info "So lets do it..."
print_info "And... done..."
echo "Line 6 - reduced awesomeness of the feature down to 100%" >> main.txt
print_info "Just commit."
echo
exec_input "git add -A && git commit -m \"Fix: reduced awesomeness back to 100%\""

echo
print_info "Because it's best pratice push your change. Even if we delete the branch in a moment."
echo
exec_input "git push origin dev_Feature"
gitlog --all

echo
print_info "Ok. Now lets get our feature branch back in line with dev."
print_info "This is realy easy..."
print_info "Just do a rebase!"
echo
exec_input "git rebase dev"

echo
print_info "Yada yada yada... yes conflict again."
print_info "Learn to live with it..."
print_info "This is because, as we already stated, both branches have the hotfix and the feature"
print_info "branch also has everything else the dev has. But the same changes have different hashes."
print_info "Guess why... yeah because of the rebase."
echo
print_info "Just discard line 2 of the feature branch and change line 3 to 4."
print_info "Let me guess? It's 'boring' isn't it?"
echo
exec_input "vi main.txt" 1
if [ $? -eq 99 ]; then
    echo "Do you even know whats in main.txt right now?"
fi
echo "Line 1 - Main App" > main.txt
echo "Line 2 - Hotfix code..." >> main.txt
echo "Line 3 - Some changes" >> main.txt
echo "Line 4 - my awesome feature" >> main.txt

echo
print_info "Continue with the rebase."
echo
exec_input "git add main.txt && git rebase --continue"

echo
print_info "Whoops... conflict... again..."
print_info "Still not getting used to it?"
echo
print_info "Drop line 2 & 3 of feature and change line 4 to 5."
print_info "If it's not too 'boring' :p"
echo
exec_input "vi main.txt" 1
if [ $? -eq 99 ]; then
    echo "Done..."
fi
echo "Line 1 - Main App" > main.txt
echo "Line 2 - Hotfix code..." >> main.txt
echo "Line 3 - Some changes" >> main.txt
echo "Line 4 - my awesome feature" >> main.txt
echo "Line 5 - improved your feature... by a thousand percent!" >> main.txt


echo
print_info "And... continue rebase."
echo
exec_input "git add main.txt && git rebase --continue"
gitlog

echo
print_info "Et voilà!"
print_info "Your feature branch is back in line with dev."
print_info "The remote counterpart of course not but since we're finished with the feature"
print_info "that branch will get deleted soon."
echo
print_info "Now lets merge our feature into dev."
print_info "Yes merge. But it's a fast forward merge."
echo
exec_input "git checkout dev"
echo
exec_input "git merge dev_Feature"
gitlog

echo
print_info "Nice. Now push and cleanup."
echo
exec_input "git push origin dev"
echo
exec_input "git branch -d dev_Feature"
echo
exec_input "git push origin :dev_Feature"
gitlog --all

echo
print_info "Well done!"
print_info "And don't worry. You'll get way less conflicts in a real project."
print_info "Since you'll have more than one file ;)"
