#!/bin/bash

echo
print_info "Setting up lecture..."
print_info "Ignore everything with blue background..."
echo
press_enter
echo -e "$bluebg"


# setup repos
cd $BASE_DIR
init_repos

# dev should be ahead of master
cd local
git checkout dev
echo "Line 2 - Some changes" >> main.txt
git add -A
git commit -m "feat: added line 2"
git push origin dev
cd ..


echo -e "$clroff"
echo
print_info "Lecture Setup completed."
print_info "Creating a simple hotfix..."
echo
echo
print_info "This is how the current dev working tree looks."

cd local
gitlog

echo
print_info "Checkout the master branch."
print_info "This is what should have been deployed to the customer..."
echo
exec_input "git checkout master"

echo
print_info "Create a hotfix branch."
echo
exec_input "git checkout -b dev_Hotfix"

echo
print_info "Now you'd do your fix."
print_info "We'll do that for you :p"
echo
print_info "Adding hotfix code to main.txt"
echo
echo "Line 2 - Hotfix code..." >> main.txt
press_enter

echo
print_info "Commit your changes."
echo
exec_input "git add -A && git commit -m \"Fix: some hotfix\""
gitlog
commit=`get_last_hash`

echo
print_info "Now you'd build and deploy the hotfix branch..."
echo
press_enter

echo
print_info "Now we need to get the hotfix back to our dev working tree"
print_info "without merging or rewriting the history."
print_info "Therefore we cherry pick the hotfix changes into dev..."
echo
print_info "Checkout the dev branch."
echo
exec_input "git checkout dev"

echo
print_info "Cherry pick the hotfix commit into dev."
echo
exec_input "git cherry-pick ${commit}"

echo
print_info "Yep... you've got a conflict..."
print_info "Lets resolve it. Put the hotfix change in line 2 and change line 2 to line 3."
echo
exec_input "vi main.txt"

echo
print_info "Commit the resolved conflict."
echo
exec_input "git add main.txt && git commit -m \"Fix: some hotfix\""

echo
print_info "Push the dev branch to origin."
echo
exec_input "git push origin dev"
gitlog --all

echo
print_info "Now you can delete the hotfix branch."
print_info "As we never merged the hotfix commit and the cherry picked commit"
print_info "has a new hash, we need to use -D"
echo
exec_input "git branch -D dev_Hotfix"
gitlog --all

echo
print_info "congrats! you made a hotfix!"
print_info "and all branches are in line!"
echo
